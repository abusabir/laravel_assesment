<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function update_user(Request $request)
    {
        $user = auth()->user();

        $this->validate($request, [
            'name' => 'required',
            'email' => "required|unique:users,email, $user->id",
            'password' => 'sometimes|nullable|min:6|confirmed'
        ]);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        if ($request->password) {
            $user->update([
                'password' => bcrypt($request->password)
            ]);
        }

        return response()->json($user, 200);
    }

    public function get_user(Request $request)
    { 
        $user = User::where('id', $request->id)->first();
        return response()->json($user, 200);
    }

    public function user_login(Request $request){
        //return response()->json($request->all());
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $user = User::where('email', $request->email)->first();

        if(!$user || !Hash::check($request->password, $user->password)){
            return response()->json([
                'status' => 401,
                'message' => 'Invalid User Name Or Password'
            ]);
        }else{
            if($user->role_as == 1){
                $token = $user->createToken($user->email.'_AdminToken',['server:admin'])->plainTextToken;
            }else{
                $token = $user->createToken($user->email.'_token',[''])->plainTextToken;
            }
            
            return response()->json([
                'status' => 200,
                'username' => $user->name,
                'user_id' => $user->id,
                'token' => $token,
                'message' => 'User Successfully Created'
            ]);
        }
    }

    public function user_registration(Request $request){
        try{
        DB::beginTransaction();

            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:8',
                'password_confirmation' => 'required|min:8',
            ]);

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);

            $token = $user->createToken($user->email.'_token')->plainTextToken;

        //Commit Database
        DB::commit();
            return response()->json([
                'status' => 200,
                'username' => $user->name,
                'token' => $token,
                'message' => 'User Successfully Created'
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'starus' => 400
            ]);

        }
    }
}

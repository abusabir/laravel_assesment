<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
          // \App\Models\User::factory(10)->create();

        DB::table('users')->insert([
            'name' => 'Admin User',
            'email' => 'admin@gmail.com',
            'role_as' => 1,
            'password' => Hash::make('password'),
        ]);

        DB::table('users')->insert([
            'name' => 'Editor User',
            'email' => 'editor@gmail.com',
            'role_as' => 0,
            'password' => Hash::make('password'),
        ]);

        //$this->call(UserSeeder::class);
    }
}

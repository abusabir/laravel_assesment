<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create(
            [
            'name' => 'Zakir Hossen',
            'email' => 'admin@gmail.com',
            'role_as' => 1,
            'password' => bcrypt('password'),
            ],
            [
            'name' => 'Zakir Hossen',
            'email' => 'customer@gmail.com',
            'role_as' => 0,
            'password' => bcrypt('password'),
            ]
        );
    }
}

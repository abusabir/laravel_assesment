<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware(['auth:sanctum'])->group(function () {

    Route::post('/user', [AuthController::class, 'get_user']);
    
    Route::resource('/category', CategoryController::class);
    Route::resource('/product', ProductController::class);

    // Route::get('/product', [ProductController::class, 'index']);
    // Route::get('/product', [ProductController::class, 'edit']);
    // Route::put('/product', [ProductController::class, 'update']);

    // Route::get('/category', [CategoryController::class, 'index']);
    // Route::get('/category', [CategoryController::class, 'edit']);
    // Route::put('/category', [CategoryController::class, 'update']);
    

});

// Route::middleware(['isAdminApi'])->group(function () {

//     Route::delete('/product', [ProductController::class, 'destroy']);
//     Route::post('/product', [ProductController::class, 'store']);

//     Route::delete('/category', [CategoryController::class, 'destroy']);
//     Route::post('/category', [CategoryController::class, 'store']);
// });


Route::post('/user_login', [AuthController::class, 'user_login']);
Route::post('/user_registration', [AuthController::class, 'user_registration']);

Route::get('products', 'PublicAPIController@products');
Route::get('products/{slug}', 'PublicAPIController@product_details');
